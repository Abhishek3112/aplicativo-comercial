import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }
  quantity = null;
  price = null;
  ip = null;
  combo = [];
  ngOnInit() {
  }

  input(v: string) {
    if (this.ip !== null) {
      if (this.ip === "quantity") {
        if (this.quantity === null) {
          this.quantity = v;
        } else {
          this.quantity = this.quantity + v;
        }
      } else if (this.ip === "price") {
        if (this.price === null) {
          this.price = v;
        } else {
          this.price = this.price + v;
        }
      }
    }
  }

  checkSum() {
    if (this.quantity != null && this.price != null) {
      let str1 = this.price;
      this.combo = str1.split("*");
      if (this.combo[1]) {
        console.log("multiplication detected");
        this.quantity = this.combo[0];
        this.price = this.combo[1];
      }
      else {
        console.log("no multiplication,direct sum");
      }
    }
  }

  onFocus(v: string) {
    if (v == "quantity") {
      this.ip = "quantity";
    } else {
      this.ip = "price";
    }
  }

  clearAll() {
    this.quantity = null;
    this.price = null;
  }

  clearInput() {
    if (this.ip === "quantity") {
      this.quantity = null;
    } else if (this.ip === "price") {
      this.price = null;
    }
  }

  removeChar() {
    if (this.ip === "email") {
      this.quantity = this.quantity.slice(0, -1);
    } else if (this.ip === "price") {
      this.price = this.price.slice(0, -1);
    }
  }
}
