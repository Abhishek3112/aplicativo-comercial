import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../providers/loader.service';
import { AlertService } from '../../providers/alert.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SignInComponent implements OnInit {

  constructor(private loaderService: LoaderService,
    private alertService: ToasterService,
    private router: Router) { }
  email = null;
  pwd = null;
  ip = null;
  public toasterConfig: ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      tapToDismiss: false,
      timeout: 0
    });


  ngOnInit() {
    // this.loaderService.display(true);
    // setTimeout(
    //   () => {
    //     this.loaderService.display(false);
    //   },8000);
  }

  success() {
    //this.alertService.pop('success', 'Hello','hi');
    this.router.navigateByUrl('home');
  }

  input(v: string) {
    if (this.ip !== null) {
      if (this.ip === "email") {
        if (this.email === null) {
          this.email = v;
        } else {
          this.email = this.email + v;
        }
      } else if (this.ip === "pwd") {
        if (this.pwd === null) {
          this.pwd = v;
        } else {
          this.pwd = this.pwd + v;
        }
      }
    }
  }

  onFocus(v: string) {
    if (v == "email") {
      this.ip = "email";
    } else {
      this.ip = "pwd";
    }
  }

  clearAll() {
    this.email = null;
    this.pwd = null;
  }

  clearInput() {
    if (this.ip === "email") {
      this.email = null;
    } else if (this.ip === "pwd") {
      this.pwd = null;
    }
  }

  removeChar() {
    if (this.ip === "email") {
      this.email = this.email.slice(0, -1);
    } else if (this.ip === "pwd") {
      this.pwd = this.pwd.slice(0, -1);
    }
  }
}
