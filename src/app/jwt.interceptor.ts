import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const JWT_TOKEN_ELEADS = localStorage.getItem('JWT_TOKEN_ELEADS');
    if (JWT_TOKEN_ELEADS) {
      request = request.clone({
        setHeaders: {
          token: JWT_TOKEN_ELEADS
        }
      });
    }

    return next.handle(request);
  }
}
