import { Injectable } from '@angular/core';
import { ToasterConfig, ToasterService } from 'angular2-toaster';

@Injectable()

export class AlertService {
  constructor(private toasterService: ToasterService) {
  }
  public config: ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      tapToDismiss: false,
      timeout: 0
    });

  popToast(type: string, body: string) {
    this.toasterService.pop('type', 'body');
  }
}
